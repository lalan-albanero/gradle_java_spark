package com.javaspark.sparkapp.controller;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.javaspark.sparkapp.model.model;
import com.javaspark.sparkapp.service.MyService;

@RestController
public class Mycontroller {
    @Autowired
    MyService myService;

    @GetMapping("/sortBy")
    ResponseEntity<?> sortby(@RequestBody model col) {
        System.out.println(col.toString());
        System.out.println("in   " + col);
        return ResponseEntity.status(HttpStatus.OK).body(myService.sortBy(col));
    }

}
