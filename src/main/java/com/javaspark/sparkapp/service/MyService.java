package com.javaspark.sparkapp.service;

import java.util.List;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import org.springframework.stereotype.Service;
import static org.apache.spark.sql.functions.col;

import com.javaspark.sparkapp.model.model;

@Service
public class MyService {

    public String sortBy(model colm) {
        System.out.println("in ser " + colm);

        SparkSession spark = SparkSession.builder().master("local").appName("ssss").getOrCreate();

        Dataset<Row> dataRead = spark.read().option("header", true).csv(
                "C:\\Users\\Lalan kumar\\Desktop\\Spark\\sparkapp\\src\\main\\resources\\static\\SampleCSVFile_11kb.csv");
        // StructType a= dataRead.schema();
        // String[] b=a.fieldNames();
        if (colm.getA() != null) {
            dataRead = dataRead.filter(col("A").equalTo(colm.getA()));
        }
        if (colm.getB() != null) {
            dataRead = dataRead.filter(col("b").equalTo(colm.getB()));

        }
        if (colm.getC() != null) {
            dataRead = dataRead.filter(col("c").equalTo(colm.getC()));
        }
        if (colm.getD() != null) {
            dataRead = dataRead.filter(col("d").equalTo(colm.getD()));
        }
        if (colm.getE() != null) {
            dataRead = dataRead.filter(col("e").equalTo(colm.getE()));
        }
        if (colm.getF() != null) {
            dataRead = dataRead.filter(col("f").equalTo(colm.getF()));
        }
        if (colm.getG() != null) {
            dataRead = dataRead.filter(col("g").equalTo(colm.getG()));
        }
        if (colm.getH() != null) {
            dataRead = dataRead.filter(col("h").equalTo(colm.getH()));
        }
        if (colm.getI() != null) {
            dataRead = dataRead.filter(col("i").equalTo(colm.getI()));
        }
        if (colm.getSort() != null) {
            dataRead = dataRead.sort(colm.getSort());
        }

        // // out.show(30);

        return dataRead.collectAsList().toString();

        // return "good evening";
    }

}
