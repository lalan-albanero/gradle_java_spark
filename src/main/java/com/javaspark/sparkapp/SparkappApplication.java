package com.javaspark.sparkapp;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparkappApplication {

	public static void main(String[] args) {
		// Logger.getLogger("org.apache").setLevel(Level.WARN);

		SpringApplication.run(SparkappApplication.class, args);
		// SparkSession spark = SparkSession.builder().master("local").appName("ssss").getOrCreate();

        // Dataset<Row> dataRead = spark.read().option("header", true).csv(
                // "C:\\Users\\Lalan kumar\\Desktop\\Spark\\sparkapp\\src\\main\\resources\\static\\SampleCSVFile_11kb.csv");
        // dataRead.show();
	}


}
